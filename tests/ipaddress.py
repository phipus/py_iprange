import unittest

from iprange import IPv4Address

class IPv4AddressTest(unittest.TestCase):
    def setUp(self):
        self.addr = IPv4Address('192.168.1.1')
        self.brodcast = IPv4Address('192.168.1.255')
        self.network = IPv4Address('192.168.1.0')
        self.cidr = 24
    
    
    def test_isnetwork(self):
        self.assertTrue(self.network.isnetwork(self.cidr))
        self.assertFalse(self.brodcast.isnetwork(self.cidr))
        self.assertFalse(self.addr.isnetwork(self.cidr))


    def test_network(self):
        self.assertEqual(self.addr.network(self.cidr), self.network)
        self.assertEqual(self.network, self.network.network(self.cidr))


    def test_broadcast(self):
        self.assertEqual(self.brodcast, self.addr.broadcast(self.cidr))


    def test_str(self):
        self.assertEqual(str(self.addr), '192.168.1.1')
        self.assertEqual(str(self.addr.network(self.cidr)), '192.168.1.0')


if __name__ == '__main__':
    unittest.main()
