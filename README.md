# IP Range

IPRange is a simple tool to generate ranges of IP-addresses. 

### Useage
    iprange 192.168.1.1 192.168.1.254
	
### Installation
    pip install iprange
